package tdd.fizzbuzz;

public class FizzBuzz {

    public static final String FIZZ = "Fizz";
    public static final String BUZZ = "Buzz";
    public static final String WHIZZ = "Whizz";

    public String countOff(int order) {
        StringBuilder word = new StringBuilder();
        if (order % 3 == 0) {
            word.append(FIZZ);
        }
        if (order % 5 == 0) {
            word.append(BUZZ);
        }
        if (order % 7 == 0) {
            word.append(WHIZZ);
        }
        if (word.length() == 0) {
            return String.valueOf(order);
        }
        return word.toString();
    }
}
