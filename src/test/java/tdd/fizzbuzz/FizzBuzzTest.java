package tdd.fizzbuzz;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTest {
    @Test
    void should_say_normal_number_when_countOff_given_normal_number() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String word = fizzBuzz.countOff(1);
        //then
        assertEquals("1", word);
    }

    @Test
    void should_say_Fizz_when_countOff_given_the_number_is_multiple_of_3() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String word = fizzBuzz.countOff(3);
        //then
        assertEquals("Fizz", word);
    }

    @Test
    void should_say_Buzz_when_countOff_given_the_number_is_multiple_of_5() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String word = fizzBuzz.countOff(10);
        //then
        assertEquals("Buzz", word);
    }

    @Test
    void should_say_Whizz_when_countOff_given_the_number_is_multiple_of_7() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String word = fizzBuzz.countOff(14);
        //then
        assertEquals("Whizz", word);
    }

    @Test
    void should_say_FizzBuzz_when_countOff_given_the_number_is_multiple_of_3_and_5() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String word = fizzBuzz.countOff(15);
        //then
        assertEquals("FizzBuzz", word);
    }

    @Test
    void should_say_FizzWhizz_when_countOff_given_the_number_is_multiple_of_3_and_7() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String word = fizzBuzz.countOff(21);
        //then
        assertEquals("FizzWhizz", word);
    }

    @Test
    void should_say_BuzzWhizz_when_countOff_given_the_number_is_multiple_of_5_and_7() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String word = fizzBuzz.countOff(35);
        //then
        assertEquals("BuzzWhizz", word);
    }

    @Test
    void should_say_FizzBuzzWhizz_when_countOff_given_the_number_is_multiple_of_3_and_5_and_7() {
        //given
        FizzBuzz fizzBuzz = new FizzBuzz();
        //when
        String word = fizzBuzz.countOff(105);
        //then
        assertEquals("FizzBuzzWhizz", word);
    }
}
